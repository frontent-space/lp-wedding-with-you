// DATA LIST NAVBAR
export const DATA_LIST_NAVBAR = {
	dataListLeft: [
		{
			id: 1,
			title: "HOME",
			path: "/"
		},
		{
			id: 2,
			title: "KATALOG",
			path: "#catalog-section"
		},
		{
			id: 3,
			title: "PORTOFOLIO",
			path: "/portofolio"
		},
		{
			id: 4,
			title: "HARGA",
			path: "/harga"
		}
	],
	dataListRight: [
		{
			id: 5,
			title: "FILTER",
			path: "/"
		},
		{
			id: 6,
			title: "FAQ",
			path: "/katalog"
		},
		{
			id: 7,
			title: "KONTAK",
			path: "/portofolio"
		},
		{
			id: 8,
			title: "TENTANG KAMI",
			path: "/about"
		},
	]

}


// DATA LIST CATALOG
export const DATA_CATALOG =
	[
		{
			template: "Best",
			id: 1,
			data: [
				{
					id: 1,
					title: "Dandelion Beauty",
					category: "Diamond",
					discount: 50,
					price: 400000,
					rating: 0,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 2,
					title: "Gold Love",
					category: "Silver",
					discount: 30,
					price: 150000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 3,
					title: "Beauty Story",
					category: "Rubby",
					discount: 50,
					price: 350000,
					rating: 4,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 4,
					title: "Happy With Me",
					category: "Silver",
					discount: 40,
					price: 150000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				}
			]
		},
		{
			template: "Gold",
			id: 2,
			data: [
				{
					id: 1,
					title: "Happy Fantastic",
					category: "Diamond",
					discount: 50,
					price: 450000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 2,
					title: "Sweatheart Gold",
					category: "Silver",
					discount: 30,
					price: 180000,
					rating: 3,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 3,
					title: "Just Like You",
					category: "Rubby",
					discount: 50,
					price: 550000,
					rating: 3,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 4,
					title: "Nobody Else",
					category: "Silver",
					discount: 40,
					price: 350000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				}
			]
		},
		{
			template: "Silver",
			id: 3,
			data: [
				{
					id: 1,
					title: "Dandelion Beauty",
					category: "Diamond",
					discount: 50,
					price: 400000,
					rating: 2,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 2,
					title: "Gold Love",
					category: "Silver",
					discount: 30,
					price: 150000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 3,
					title: "Beauty Story",
					category: "Rubby",
					discount: 50,
					price: 350000,
					rating: 1,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 4,
					title: "Happy With Me",
					category: "Silver",
					discount: 40,
					price: 150000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				}
			]
		},
		{
			template: "Rubby",
			id: 4,
			data: [
				{
					id: 1,
					title: "Dandelion Beauty",
					category: "Diamond",
					discount: 50,
					price: 400000,
					rating: 3,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 2,
					title: "Gold Love",
					category: "Silver",
					discount: 30,
					price: 150000,
					rating: 4,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 3,
					title: "Beauty Story",
					category: "Rubby",
					discount: 50,
					price: 350000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 4,
					title: "Happy With Me",
					category: "Silver",
					discount: 40,
					price: 150000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				}
			]
		},
		{
			template: "Diamond",
			id: 5,
			data: [
				{
					id: 1,
					title: "Dandelion Beauty",
					category: "Diamond",
					discount: 50,
					price: 400000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 2,
					title: "Gold Love",
					category: "Silver",
					discount: 30,
					price: 150000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 3,
					title: "Beauty Story",
					category: "Rubby",
					discount: 50,
					price: 350000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 4,
					title: "Happy With Me",
					category: "Silver",
					discount: 40,
					price: 150000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				}
			]
		},
		{
			template: "Syari",
			id: 6,
			data: [
				{
					id: 1,
					title: "Dandelion Beauty",
					category: "Diamond",
					discount: 50,
					price: 400000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 2,
					title: "Gold Love",
					category: "Silver",
					discount: 30,
					price: 150000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 3,
					title: "Beauty Story",
					category: "Rubby",
					discount: 50,
					price: 350000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				},
				{
					id: 4,
					title: "Happy With Me",
					category: "Silver",
					discount: 40,
					price: 150000,
					rating: 5,
					image: "/assets/dummy-catalog-image.png",
					url: "demo-undangan"
				}
			]
		}
	];

	// DATA LIST FAQ
export const DATA_PRICELIST =
[
	{
		id: 1,
		package: "Silver",
		price: 400000,
		benefit: [
			{
				data: "Maksimal 5 Foto"
			},
			{
				data: "Fitur Kepada"
			},
			{
				data: "Unlimited Share"
			},
			{
				data: "Navigasi Maps"
			},
			{
				data: "Hitung Mundur"
			},
			{
				data: "Ganti Tone Warna"
			},
			{
				data: "Amplop Online (Optional)"
			},
			{
				data: "Kolom Ucapan"
			},
			{
				data: "Musik / Theme Songs"
			},
		]
	},
	{
		id: 2,
		package: "Gold",
		price: 350000,
		benefit: [
			{
				data: "Maksimal 5 Foto"
			},
			{
				data: "Fitur Kepada"
			},
			{
				data: "Unlimited Share"
			},
			{
				data: "Meida social link"
			},
			{
				data: "Navigasi Maps"
			},
			{
				data: "Hitung Mundur"
			},
			{
				data: "Ganti Tone Warna"
			},
			{
				data: "Amplop Online (Optional)"
			},
			{
				data: "Kolom Ucapan"
			},
			{
				data: "Musik / Theme Songs"
			},
		]
	},
	{
		id: 3,
		package: "Ruby",
		price: 455000,
		benefit: [
			{
				data: "Maksimal 5 Foto"
			},
			{
				data: "Fitur Kepada"
			},
			{
				data: "Unlimited Share"
			},
			{
				data: "Meida social link"
			},
			{
				data: "Navigasi Maps"
			},
			{
				data: "Hitung Mundur"
			},
			{
				data: "Ganti Tone Warna"
			},
			{
				data: "Amplop Online (Optional)"
			},
			{
				data: "Kolom Ucapan"
			},
			{
				data: "Musik / Theme Songs"
			},
			{
				data: "Reservasi kehadiran"
			},
		]
	},
	{
		id: 4,
		package: "Diamond",
		price: 500000,
		benefit: [
			{
				data: "biasa aja"
			},
			{
				data: "dimana senang"
			},
			{
				data: "suka musik"
			},
			{
				data: "suka suka aja"
			},
		]
	},
	{
		id: 5,
		package: "No Image",
		price: 400000,
		benefit: [
			{
				data: "Gratis domain"
			},
			{
				data: "Fitur google maps location"
			},
			{
				data: "Fitur Live music"
			},
			{
				data: "Fitur Donation Bank"
			},
		]
	}
];

// DATA LIST FAQ
export const DATA_FAQ =
	[
		{
			id: 1,
			question: "Berapa lama proses pembuatan undangan ?",
			answer: "Lama proses pengerjan 2-3 hari tergantung kelengkapan data yang diberikan oleh client. jika tidak ada kendala pada kelengkapan data tim dapat mengerjakan tepat pada waktu yang telah ditentukan"
		},
		{
			id: 2,
			question: "Berapa lama masa aktif undangan ?",
			answer: "Undangan akan aktif minimal 1 bulan sebelum acara jika client ingin menyiapkan acara lebih awal dan 1 bulan setelah acara pernikahan"
		},
		{
			id: 3,
			question: "Bagaimana proses revisi ?",
			answer: "Revisi akan dikerjakan selama 1-2 hari tergantung kelengkapan data dan revisi hanya berupa text, gambar. Sedangkan design yang telah ada tidak dapat direvisi"
		},
		{
			id: 3,
			question: "Bagaimana proses revisi ?",
			answer: "Revisi akan dikerjakan selama 1-2 hari tergantung kelengkapan data dan revisi hanya berupa text, gambar. Sedangkan design yang telah ada tidak dapat direvisi"
		},
		{
			id: 3,
			question: "Apakah bisa refund ?",
			answer: "Mohon maaf karena kita tidak bisa melakukan refund karena transaksi yang dilakukan berdasarkan kesepatan yang pasti diawal"
		},{
			id: 3,
			question: "Persetujuan ?",
			answer: "Dengan memesan di kami, berarti Anda telah menyetujui jika dikemudian hari, materi undangan dan foto dapat kami gunakan sebagai portofolio sebagai bahan promosi kami."
		}

	];



