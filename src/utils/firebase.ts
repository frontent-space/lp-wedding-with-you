// Import the functions you need from the SDKs you need
"use client"

import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCg-C6WaEAdty5oj8VWu570qDzSLP0hwuQ",
  authDomain: "wedding-with-you.firebaseapp.com",
  projectId: "wedding-with-you",
  storageBucket: "wedding-with-you.appspot.com",
  messagingSenderId: "251486490601",
  appId: "1:251486490601:web:69e520b613e46a4f99c578",
  measurementId: "G-REDP2KNEZZ"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
