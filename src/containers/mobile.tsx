import React, { ReactNode } from 'react'

interface MobileProps {
  children?: ReactNode;
}

export default function mobile({ children }: MobileProps) {
  return (
    <main className="md:flex md:justify-center">
      <div className="xs:w-full md:w-[450px]">
        {children}
      </div>  
    </main>
  )
}