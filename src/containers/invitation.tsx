import React, { ReactNode } from 'react'

interface InvitationProps {
	coverChild?: ReactNode;
	invitationChild?: ReactNode;
}

export default function invitation({ coverChild, invitationChild }: InvitationProps) {
	return (
		<main className="flex flex-row min-w-full">
			<div className="xs:hidden md:block sm:block w-[calc(100vw-500px)] box-border h-full min-h-screen overflow-hidden left-0 top-0 fixed z-10">
				{coverChild}
			</div>
			<div className="w-[500px] bg-red-400 box-border h-auto z-20 absolute right-0 overflow-hidden">
			{invitationChild}
			</div>
		
		</main>
	)
}

