"use client"

import React, {useState} from 'react'
import { ReactQueryDevtools } from 'react-query/devtools'
import { useQuery, useMutation, useQueryClient, QueryClient, QueryClientProvider} from 'react-query'

export default function TanstackProviders({children}: {children: React.ReactNode}) {
    const queryClient = new QueryClient()
  return (
    <QueryClientProvider client={queryClient}>
        {children}
        <ReactQueryDevtools initialIsOpen={false}></ReactQueryDevtools>
    </QueryClientProvider>
  )
}
