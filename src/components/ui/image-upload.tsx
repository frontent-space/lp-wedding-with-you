import * as React from "react";
import { cn } from "@/lib/utils";

export interface ImageUploadProps extends React.InputHTMLAttributes<HTMLInputElement> { }

const ImageUpload = React.forwardRef<HTMLInputElement, ImageUploadProps>(
  ({ className, ...props }, ref) => {
    const inputRef = React.useRef<HTMLInputElement | null>(null);

    const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
      // Handle file change event if needed
      console.log('File changed:', e.target.files);
    };

    return (
      <>
        <div className="relative bg-[#F5F5F5] rounded-lg h-[90px] p-[14px]">
          <div className="flex flex-row justify-center items-center border-dashed border rounded-xl w-full h-full">
            <input
              type="file"
              className="absolute opacity-0"
              ref={(input) => {
                inputRef.current = input;
                if (ref) {
                  if (typeof ref === 'function') {
                    ref(inputRef.current);
                  } else {
                    ref.current = inputRef.current as HTMLInputElement;
                  }
                }
              }}
              onChange={handleFileChange}
              {...props}
            />
            <label
              className={cn(
                "flex flex-row items-center space-x-2",
                className
              )}
              htmlFor={props.id || 'file-input'}
            >
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#717179" className="w-5 h-5">
                <path stroke-linecap="round" stroke-linejoin="round" d="M12 16.5V9.75m0 0 3 3m-3-3-3 3M6.75 19.5a4.5 4.5 0 0 1-1.41-8.775 5.25 5.25 0 0 1 10.233-2.33 3 3 0 0 1 3.758 3.848A3.752 3.752 0 0 1 18 19.5H6.75Z" />
              </svg>


              <p className="text-[#717179] text-xs mt-1">Upload Fofo Profile</p>


            </label>

          </div>

        </div>
       

      </>
    );
  }
);

ImageUpload.displayName = "ImageUpload";

export { ImageUpload };
