"use client"

import React, { ReactNode, useState, useEffect } from 'react'
import Image, { ImageProps } from 'next/image';

interface InvitationProps {
  children?: ReactNode;
  image: string[];
  className: string;
}

export default function Background({ children, image, className }: InvitationProps) {
  console.log("image ada apa", image)

  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setActiveIndex((prevIndex) => (prevIndex + 1) % image.length);
    }, 7000);

    return () => clearInterval(interval);
  }, []);


  return (
    // <div style={{backgroundImage: `url(${currentBackground})`}} className={`w-full xs:h-[100dvh] bg-slate-300 absolute ${className}`}>
    //     {children}
    // </div>

    <>
      {image.map((path, index) => (
        <div key={index} style={{backgroundImage: `url(${[path]})`}} className={`w-full h-[100dvh] bg-slate-300 sliderImage ${index === activeIndex ? "active" : ''} ${className}`}>
          {children}
        </div>
      ))}
      {children}
    </>
  )
}
