"use client"

import React, { useState, useEffect } from 'react';

const imagePaths = [
    'https://images.unsplash.com/photo-1679679195912-29d0190805ad?q=80&w=2835&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
  ];

function BackgroundSlider() {
  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setActiveIndex((prevIndex) => (prevIndex + 1) % imagePaths.length);
    }, 7000);

    return () => clearInterval(interval);
  }, []);

  return (
    <div className="backgroundSlider">
      {imagePaths.map((path, index) => (
        <img
          key={index}
          src={path}
          alt={`Image ${index + 1}`}
          className={`sliderImage ${index === activeIndex ? "active" : ''}`}
        />
      ))}
    </div>
  );
}

export default BackgroundSlider;
