import React from 'react'
import Container from '@/containers/mobile'
import BackgroundImage from '@/components/backgroundImage'

export default function page() {
  return (
   <Container children={
    <BackgroundImage/>
   }/>
  )
}
