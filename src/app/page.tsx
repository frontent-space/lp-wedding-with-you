
"use client"
import { useEffect, useState } from 'react'
import Image from 'next/image'
// import Button from '@/components/Button'
import Nav from '@/app/(landingPage)/(section)/Navbar'
import Hero from './(landingPage)/(section)/Hero'
import Catalog from './(landingPage)/(section)/Catalog'

import { collection, getDocs, getFirestore } from 'firebase/firestore';
import { logEvent, setUserId, getAnalytics } from "firebase/analytics";
import {app} from '@/utils/firebase'


export default function Home() {
  // const analytics = getAnalytics(app);

  const [data, setData] = useState({});

  const db = getFirestore(app);

  useEffect(() => {
    // Contoh: Log event saat komponen dimuat
    // logEvent(analytics, 'page_view', { page_path: '/my-component' });

    // Contoh: Set user ID saat pengguna masuk
    // setUserId(analytics, 'user123');
  }, []); // Mengeksekusi efek hanya sekali setelah komponen dimuat
  

  useEffect(() => {
   
    const fetchData = async () => {
      try {
        
        const querySnapshot = await getDocs(collection(db, "invitationDetail"));
        // console.log("query", querySnapshot)
     
        querySnapshot.forEach((doc) => {
          console.log(`${doc.id} => ${JSON.stringify(doc.data())}`);
          setData(doc.data())
          const udel = doc.data()
          console.log("udel", udel)
        });

      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  return (
    <>
    {console.log('dataaaaaa', data)}
      <Nav />
      <main>
        <Hero />
        <Catalog />
      </main>
    </>

  )
}


