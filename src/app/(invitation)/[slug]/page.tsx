"use client"

import React, { useState, useEffect } from 'react'
import SVL01 from '@/templates/silver/Silver01'
type InvitationPage = { params: { slug: string } };
import { collection, getDocs, getFirestore } from 'firebase/firestore';
import { app } from '@/utils/firebase'

interface InvitationData {
    path?: string;
    id?: string;
    // Tambahkan properti lain jika ada lebih banyak field dalam data Anda
  }

export default function page(props: InvitationPage) {
    const { params } = props;
    const [data, setData] = useState<InvitationData>({});
  
    const db = getFirestore(app);

    useEffect(() => {

        const fetchData = async () => {
            try {

                const querySnapshot = await getDocs(collection(db, "invitationDetail"));
                // console.log("query", querySnapshot)

                querySnapshot.forEach((doc) => {
                    console.log(`${doc.id} => ${JSON.stringify(doc.data())}`);
                    setData(doc.data())
                    const udel = doc.data()
                    // console.log("udel", udel)
                });

            } catch (error) {
                console.error("Error fetching data:", error);
            }
        };

        fetchData();
    }, []);

    return (
        <>
            {params.slug === data.path &&
                (
                    data.id === "DM01" ? <SVL01 dataDetail={data} />
                        : "")

            }
        </>
    )
}
