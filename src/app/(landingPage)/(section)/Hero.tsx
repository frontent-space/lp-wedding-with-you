import React from 'react'
import Image from 'next/image'
import { Volkhov, Poppins } from 'next/font/google'

const volkhov = Volkhov({
    subsets: ['latin'],
    weight: '700',
    variable: '--font-volkhov',
})

const poppins = Poppins({
    subsets: ['latin'],
    display: 'swap',
    variable: '--font-poppins',
    weight: ['100', '200', '300', '400', '500', '600', '700', '800', '900']
});

export default function Hero() {
    const styleInfoDetail = `inline-block leading-4 xs:sm:text-[10px] md:text-xs`
    const styleInfoNumber = `inline-block font-semibold xs:sm:text-[28px] md:text-4xl`

    return (
        <section id="home">
            <div className="w-full max-w-[992px] py-10 m-auto flex flex-col justify-center items-center">
                <div className="flex flex-col items-center justify-center">
                    <div className="flex flex-row items-center justify-center">
                        <Image src="/assets/verified.svg" alt="Verified by Apudi" width={28} height={28} />
                        <p className="mr-3 xs:text-xs sm:text-sm md:text-md">Verified By</p>
                        <Image src="/assets/apudi-logo.webp" alt="Verified by Apudi" width={81} height={34}></Image>
                    </div>
                    <h1 className={`${volkhov.className} text-center mt-4 leading-tight xs:text-2xl xs:max-w-[270px] sm:text-[38px] sm:max-w-2xl md:text-5xl md:max-w-2xl`}>Wujudkan Pernikahan Impian Anda Bersama Kami</h1>
                    <p className={`${poppins.variable} text-center mt-5 xs:text-xs xs:max-w-[270px] xs:leading-6 sm:text-sm sm:px-56 sm:max-w-4xl sm:leading-7 md:text-lg md:max-w-4xl leading-7`}>Dapatkan pengalaman yang tak terlupakan dengan tema undangan elegan dan fitur terbaik dari kami hanya untuk anda</p>
                    <button className="bg-[#B67944] text-white rounded-[48px] mt-10 x:text-sm xs:w-32 xs:h-10 sm:text-sm sm:w-40 sm:h-11 sm:py-2 sm:px-3 md:text-lg md:w-[218px] md:h-14 md:py-2 md:px-3">
                        CEK KATALOG
                    </button>
                    <div className="text-[#363636] mt-[38px] p-3">
                        <div className="flex flex-row items-center justify-center">
                            <div className="space-x-2 px-8">
                                <p className={styleInfoNumber}>100+</p>
                                <p className={styleInfoDetail}>Testimoni<br />Pengguna</p>
                            </div>
                            <div className="space-x-2 border-x-[1px] px-8 border-zinc-400">
                                <p className={styleInfoNumber}>199+</p>
                                <p className={styleInfoDetail}>Undangan<br />Disebar</p>
                            </div>
                            <div className="space-x-2 px-8">
                                <p className={styleInfoNumber}>30+</p>
                                <p className={styleInfoDetail}>Template<br />Premium</p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </section>
    )
}
