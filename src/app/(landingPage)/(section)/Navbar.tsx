"use client"

import React, { useState, useEffect } from 'react'

import Link from 'next/link';
import Image from 'next/image';
import { Bars3BottomRightIcon, XMarkIcon } from '@heroicons/react/24/solid'
import { DATA_LIST_NAVBAR } from '@/utils/textBank'

export default function Navbar() {
    const [scrolled, setScrolled] = useState(false)
    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        const handleScroll = () => {
            setScrolled(window.pageYOffset > 10)
        }

        window.addEventListener("scroll", handleScroll)
        return () => {
            window.removeEventListener("scroll", handleScroll)
        }
    }, [])

    return (
        <nav className="sticky top-0 z-50 bg-white bg-opacity-70 backdrop-blur-lg w-full">
            <div className={`flex items-center xs:sm:justify-start md:justify-center xs:h-16 sm:h-20 md:h-28 xs:px-6 sm:px-8 xs:sm:shadow ${scrolled ? "md:shadow" : "md:shadow-none"}`}>
                <div className="flex items-center w-full xs:sm:justify-start md:justify-center">
                    <div className="xs:hidden sm:hidden md:block">
                        <ul className="flex flex-item-baseline space-x-8">
                            {DATA_LIST_NAVBAR.dataListLeft.map((listMenu, index) => (
                                <li key={index}>
                                    <Link
                                        className="font-normal text-sm hover:text-[#B67944] app-subnav__link"
                                        href={listMenu.path}>
                                        {listMenu.title}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                    <div className="flex items-center flex-shrink-0 xs:sm:mx-0 xs:sm:justify-start xs:w-[74px] sm:w-[-90px] md:mx-[7%] md:justify-center md:w-[112px]">
                        <Image priority fetchPriority='high' className="cursor-pointer" src="/assets/logo-wedding-with-you.svg" alt="Logo Wedding With You" width="0" height="0" sizes="100vw" style={{ width: '100%', height: 'auto' }} />
                    </div>
                    <div className="xs:hidden sm:hidden md:block">
                        <ul className="flex flex-item-baseline space-x-8">
                            {DATA_LIST_NAVBAR.dataListRight.map((listMenu, index) => (
                                <li key={index}>
                                    <Link
                                        className="font-normal text-sm hover:text-[#B67944] app-subnav__link"
                                        href={listMenu.path}>
                                        {listMenu.title}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
                <div className="xs:sm:block md:hidden">
                    <button
                        type="button"
                        onClick={() => setIsOpen(!isOpen)}
                        className="inline-flex items-center justify-center py-2 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-wdwu-white hover:bg-wdwu-white"
                        aria-controls="mobile-menu"
                        aria-expanded="false"
                    >
                        <span className="sr-only">Open main menu</span>
                        {!isOpen ? (
                            <Bars3BottomRightIcon
                                className="h-5 w-5"
                            />
                        ) : (
                            <XMarkIcon className="h-5 w-5"
                            />
                        )}
                    </button>

                </div>
            </div>
            {isOpen &&
                <div className="xs:block md:hidden space-y-1 shadow p-6">
                    <ul className="flex flex-col items-center">
                        {DATA_LIST_NAVBAR.dataListLeft.concat(DATA_LIST_NAVBAR.dataListRight).map((listMenu, index) => {
                            return (
                                <li key={index} className="p-3 font-normal">
                                    <Link
                                        className="font-normal app-subnav__link hover:text-[#B67944]"
                                        href={listMenu.path}
                                        scroll={false}
                                        as="#catalog-section"
                                    >
                                        {listMenu.title}
                                    </Link>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            }
        </nav>
    )
}
