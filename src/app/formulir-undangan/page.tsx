
"use client"
import React, { useState, useEffect } from 'react'
import Container from '@/containers/mobile'
import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { OrderForm } from './(step)/order-form'
import FirstPage from './(step)/first-page'
import ProfileForm from './(step)/profile-form'
import { InvitationThemeForm } from './(step)/invitation-theme-form'
import InviationFeatureForm from './(step)/invitation-feature-form'

import Cus from './(step)/cus'

export default function page() {
  const [currentStep, setCurrentStep] = useState(1);

  const goToNextStep = () => {
    setCurrentStep(currentStep + 1);
  };

  const renderForm = () => {
    switch (currentStep) {
      case 1:
        return <FirstPage onNext={goToNextStep}/>;
      case 2:
        return <OrderForm onNext={goToNextStep} />;
      case 3:
        return <InvitationThemeForm onNext={goToNextStep} />;
      case 4:
        return <InviationFeatureForm onNext={goToNextStep} />;
      case 5:
          return <ProfileForm onNext={goToNextStep} />;
      case 6:
        return <InvitationThemeForm onNext={goToNextStep}/>;
      // Tambahkan langkah formulir berikutnya jika diperlukan
      default:
        return null;
    }
  };

  return (
    <Container children={
      <>
        <div className="borderku">
          {renderForm()}
        </div>
      </>

    } />
  )
}
