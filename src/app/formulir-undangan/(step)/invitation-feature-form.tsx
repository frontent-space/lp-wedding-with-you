import React from 'react'
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { Switch } from "@/components/ui/switch"


import * as z from "zod"

import { Button } from "@/components/ui/button"
import {
	Form,
	FormControl,
	FormDescription,
	FormField,
	FormItem,
	FormLabel,
	FormMessage,
} from "@/components/ui/form"

interface Props {
	onNext: () => void;
}

const FormSchema = z.object({
	paket: z.string(),
	tema: z.string(),
	phone_number: z.string(),
	email: z.string(),
	referral_code: z.string(),
	fitur: z.boolean().default(false).optional(),

})

export default function InviationFeatureForm({ onNext }: Props) {

	const handleNextClick = (): void => {

		onNext();
	};

	const form = useForm<z.infer<typeof FormSchema>>({
		resolver: zodResolver(FormSchema),
		defaultValues: {
			paket: "",
			tema: "",
			phone_number: "",
			email: "",
			referral_code: "",
			fitur: false,
		},
	})

	function onSubmit(data: z.infer<typeof FormSchema>) {
		console.log("cek data", data)
		handleNextClick()
	}

	return (
		<>
			<div className="pt-14 py-14 px-6">
				<p className="text-center text-xs text-[#71717A] font-normal uppercase">STEP 2/10</p>
				<h5 className="text-center text-md font-semibold uppercase">PAKET DAN TEMA UNDANGAN</h5>
				<p className="text-center text-[#71717A] text-sm font-normal">Tema undangan yang tersedia
					berdasarkan <br /> paket undangan yang dipilih</p>
			</div>
			<Form {...form}>
				<form onSubmit={form.handleSubmit(onSubmit)}>
					<div className="px-6 w-full pb-[150px]">
						<div className="mb-5 space-y-[2px]">
							<h5 className=" text-base font-semibold">Paket Undangan</h5>
							<p className="text-[#71717A] text-sm font-normal">Sentuh tab untuk memilih paket undangan</p>
						</div>
						<div className="grid grid-cols-3 gap-3">

							<div className="w-full h-[83px] border rounded-lg">	1 </div>
							<div className="w-full h-[83px] border rounded-lg">	2 </div>
							<div className="w-full h-[83px] border rounded-lg">	3 </div>
							<div className="w-full h-[83px] border rounded-lg">	4 </div>
							<div className="w-full h-[83px] border rounded-lg">	5 </div>
							<div className="w-full h-[83px] border rounded-lg">	6 </div>

						</div>
						<div className="mt-10 mb-5 space-y-[2px]">
							<h5 className=" text-base font-semibold">Tema Sesuai Paket</h5>
							<p className="text-[#71717A] text-sm font-normal">Sentuh tab untuk memilih paket undangan</p>
						</div>
						<FormField
							control={form.control}
							name="fitur"
							render={({ field }) => (
								<FormItem className="flex flex-row items-center justify-between rounded-2xl p-4 h-[65px] bg-[#F8F8FB]">
									<div className="">
										
									</div>
									<div className="flex flex-col justify-center space-x-0">
									<FormLabel className="text-sm font-medium" style={{lineHeight: "20px"}}>
											ommunication emails
										</FormLabel>
										<FormDescription className="text-sm leading-0" style={{lineHeight: "20px"}}>
											Receive emails about your account activity.
										</FormDescription>
									</div>
									{/* <div className="" style={{margin: "0px"}}>
										<FormLabel className="text-sm font-medium" style={{lineHeight: "10px"}}>
											ommunication emails
										</FormLabel>
										<FormDescription className="text-sm leading-0" style={{lineHeight: "10px"}}>
											Receive emails about your account activity.
										</FormDescription>
									</div> */}
									<FormControl>
										<Switch
											checked={false}
											onCheckedChange={field.onChange}
										/>
									</FormControl>
								</FormItem>
							)}
						/>
						<div className="space-y-7">
						<FormField
							control={form.control}
							name="fitur"
							render={({ field }) => (
								<FormItem className="flex flex-row items-center justify-between rounded-2xl p-4 h-[65px] bg-[#F8F8FB]">
									<div className="">
										
									</div>
									<div className="flex flex-col justify-center space-x-0">
									<FormLabel className="text-sm font-medium" style={{lineHeight: "20px"}}>
											ommunication emails
										</FormLabel>
										<FormDescription className="text-sm leading-0" style={{lineHeight: "20px"}}>
											Receive emails about your account activity.
										</FormDescription>
									</div>
									{/* <div className="" style={{margin: "0px"}}>
										<FormLabel className="text-sm font-medium" style={{lineHeight: "10px"}}>
											ommunication emails
										</FormLabel>
										<FormDescription className="text-sm leading-0" style={{lineHeight: "10px"}}>
											Receive emails about your account activity.
										</FormDescription>
									</div> */}
									<FormControl>
										<Switch
											checked={false}
											onCheckedChange={field.onChange}
										/>
									</FormControl>
								</FormItem>
							)}
						/>
						</div>
					</div>
					<div className="w-full h-[105px] px-[24px] pt-4 mb-auto z-50 sticky bottom-0 left-0 right-0 bg-opacity-80 backdrop-filter backdrop-blur-lg border-t border-[#F5F5F5] mt-[-105px]">
						<Button type="submit" size="primary">
							Selanjutnya
						</Button>
					</div>
				</form>
			</Form>

		</>
	)
}
