"use client"

import React from 'react'
import { Button } from "@/components/ui/button"
import Image from 'next/image'

interface FirstPageProps {
    onNext: () => void;
  }

  

export default function FirstPage({onNext}: FirstPageProps) {
  
    const handleNextClick = (): void => {
        console.log("dilakukan ga")
        // Lakukan validasi formulir jika diperlukan
        // ...
    
        // Panggil onNext untuk berpindah ke langkah berikutnya
        onNext();

      };
    return (
        <>
            <div className="flex flex-col justify-end h-[100dvh] gap-10 p-[24px]">
                <div className="flex justify-center mb-5">
                <Image src="/assets/image/image-dummy.png" alt="Logo Wedding With You" width={250} height={250} />
                </div>
                <div className=" space-y-4">
                    <h5 className="text-[36px] font-bold text-center leading-10">Formulir <br/>Undangan Digital</h5>
                    <p className="text-lg font-normal text-[#71717A] text-center">Lengkapi formulir undangan untuk memulai proses pengerjaan</p>
                </div>
                <Button size={"primary"} onClick={handleNextClick} >Selanjutnya</Button>
            </div>
        </>
    )
}
