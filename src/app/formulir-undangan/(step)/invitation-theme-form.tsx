"use client"

import { useState } from "react"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group"
import { Label } from "@/components/ui/label"
import { FaceIcon, ImageIcon, SunIcon } from '@radix-ui/react-icons'
import { Icons } from "@/components/ui/icons"
import { CheckIcon } from "@radix-ui/react-icons"
import { useQuery } from "react-query"
import { fetch } from "@/lib/axios"
import Image from "next/image"

import * as z from "zod"

import { Button } from "@/components/ui/button"
import {
	Form,
	FormControl,
	FormDescription,
	FormField,
	FormItem,
	FormLabel,
	FormMessage,
} from "@/components/ui/form"
import {
	Tabs,
	TabsContent,
	TabsList,
	TabsTrigger,
} from "@/components/ui/tabs"
import { Input } from "@/components/ui/input"
import { toast } from "@/components/ui/use-toast"
import { useEffect } from "react"

interface Props {
	onNext: () => void;
}


const FormSchema = z.object({
	paket: z.string(),
	tema: z.string(),
	phone_number: z.string(),
	email: z.string(),
	referral_code: z.string(),

})

const payload = {
	data_pemesan: {
		nama: "Roni",
		no_hp: "082344557386",
		email: "roniseptadi@gmail.com",
		kode_referral: "SZE01"
	},
	tema_undangan: {
		id_paket: "n8348db83yrf7834ydf",
		id_tema: "89834jsd7834hdf87"
	},
	profile_mempelai: {
		pria: {
			nama_lengkap: "Roni Septadi",
			nama_panggilan: "Roni",
			ig: "ronisept",
			foto_profile: "base64",
			nama_ayah: "Praboni raharja spd",
			nama_ibu: "mega laksmi pdi"
		},
		wanita: {
			nama_lengkap: "Rodiah Sukmatari",
			nama_panggilan: "Tari",
			ig: "rrstari",
			foto_profile: "base64",
			nama_ayah: "Ginanjar Rohmat",
			nama_ibu: "Ani Aminah"
		},
		rangkaian_acara: {
			akad: {
				tgl_acara: "2022-07-31",
				jam_acara: "14.00",
				tempat_acara: "Mesjid Istiqlal",
				alamat_acara: "Jln Permata Indah, Tegal Raya, Kab. Tegal",
				link_gmaps: "https://maps.app.goo.gl/1Evqx7bM3JsdhXyi6",
				link_livestreaming: "https://meet.google.com/mxw-cama-axk"
			},
			resepsi: {
				pria: {
					is_active: true,
					tgl_acara: "2022-07-31",
					jam_acara: "14.00",
					tempat_acara: "Mesjid Istiqlal",
					alamat_acara: "Jln Permata Indah, Tegal Raya, Kab. Tegal",
					link_gmaps: "https://maps.app.goo.gl/1Evqx7bM3JsdhXyi6",
					link_livestreaming: "https://meet.google.com/mxw-cama-axk"
				},
				wanita: {
					is_active: true,
					tgl_acara: "2022-07-31",
					jam_acara: "14.00",
					tempat_acara: "Mesjid Istiqlal",
					alamat_acara: "Jln Permata Indah, Tegal Raya, Kab. Tegal",
					link_gmaps: "https://maps.app.goo.gl/1Evqx7bM3JsdhXyi6",
					link_livestreaming: "https://meet.google.com/mxw-cama-axk"
				},
				lainya: {
					judul_acara: "Private Wedding Party",
					tgl_acara: "",
					jam_acara: "",
					tempat_acara: "Mesjid Istiqlal",
					alamat_acara: "Jln Permata Indah, Tegal Raya, Kab. Tegal",
					link_gmaps: "https://maps.app.goo.gl/1Evqx7bM3JsdhXyi6",
					link_livestreaming: "https://meet.google.com/mxw-cama-axk"
				},
				love_story: {
					is_active: true,
					story: [
						{
							judul: "Awal Perkenalan",
							text: "Awalnya kami tidak saling kenal padahal kami satu sekolah..."
						},
						{
							judul: "Mengikat Janji",
							text: "Akhirnya kami menjadi pasangan suamu isri"
						}
					]
				},
				wedding_gift: {
					is_active: true,
					bank: [
						{
							id_bank_e_wallet: "BCA",
							no_rekening: "09283894723"
						}
					],
					alamat_kado: {
						tempat: "Rumah Mempelai Pria",
						alamat: "Jln Ketapang Raya no 56, Menado Selatan"
					}

				},
				galery: {
					link_download: "https://drive.google.com"
				},
				pengumuman: {
					is_active: false,
					// kedepanya akan ada tapi, sekarang belum
				},
				barcode_kehadiran: {
					is_active: false,
					// kedepanya akan ada tapi, sekarang belum
				}
			}
		}

	}

}

export function InvitationThemeForm({ onNext }: Props) {
	const [themeChecked, setThemeChecked] = useState("");
	const [tierChecked, setTierChecked] = useState("");

	console.log("themeChecked", themeChecked)


	const { data, error, refetch, isError, isLoading } = useQuery({
		queryKey: ['myData'],
		queryFn: async () => {
			const response = await fetch.get('api/v1/tema/6db73886-4ff7-4813-b2f8-edd141f66560/detail')
			return response;
		},
		refetchOnWindowFocus: false,
	})

	const state = data?.data;

	console.log("cek data", data)
	const handleNextClick = (): void => {

		onNext();
	};

	const textDummy = {
		data: [
			{
				paket: "Gold",
				id: 1
			},
			{
				paket: "No Image",
				id: 2
			},
			{
				paket: "Ruby",
				id: 3
			},
			{
				paket: "Diamond",
				id: 4
			},

		]
	}


	const form = useForm<z.infer<typeof FormSchema>>({
		resolver: zodResolver(FormSchema),
		defaultValues: {
			paket: "",
			tema: "",
			phone_number: "",
			email: "",
			referral_code: "",
		},
	})

	function onSubmit(data: z.infer<typeof FormSchema>) {
		console.log("cek data", data)
		toast({
			title: "You submitted the following values:",
			description: (
				<pre className="mt-2 w-[340px] rounded-md bg-slate-950 p-4">
					<code className="text-white">{JSON.stringify(data, null, 2)}</code>
				</pre>
			),
		})
		handleNextClick()
	}

	return (
		<>
			<div className="pt-14 py-14 px-6">
				<p className="text-center text-xs text-[#71717A] font-normal uppercase">STEP 2/10</p>
				<h5 className="text-center text-md font-semibold uppercase">PAKET DAN TEMA UNDANGAN</h5>
				<p className="text-center text-[#71717A] text-sm font-normal">Tema undangan yang tersedia
					berdasarkan <br /> paket undangan yang dipilih</p>
			</div>
			<Form {...form}>
				<form onSubmit={form.handleSubmit(onSubmit)}>
					<div className="px-6 w-full pb-[150px]">
						<div className="mb-5 space-y-[2px]">
							<h5 className=" text-base font-semibold">Paket Undangan</h5>
							<p className="text-[#71717A] text-sm font-normal">Sentuh tab untuk memilih paket undangan</p>
						</div>
						<FormField
							control={form.control}
							name="paket"
							render={({ field }) => (
								<FormItem>
									<FormControl>
										<RadioGroup defaultValue="card" className="grid grid-cols-2 grid-rows-2 gap-3">
											{textDummy.data.map((data: any, index: number) => (
												<div key={index}>
													<RadioGroupItem value={data.paket} id={data.paket} onClick={() => setTierChecked(data.paket)} className="peer sr-only" />
													<Label
														htmlFor={data.paket}
														className="relative flex flex-row w-full h-[75px] items-center space-x-2 rounded-lg border-2 border-muted bg-popover py-4 pl-6 pr-4 hover:bg-accent hover:text-accent-foreground peer-data-[state=checked]:border-primary [&:has([data-state=checked])]:border-primary"
													>
														<Icons.paypal className="h-6 w-6" />
														<p className="text-sm font-medium">{data.paket}</p>

														<div className={`absolute  right-0 top-0 rounded-bl-full bg-primary p-1 pb-2 pl-2 text-white ${tierChecked === data.paket ? "absolute" : "hidden"}`}>
															<CheckIcon className="h-4 w-4 text-white" />
														</div>
													</Label>
												</div>
											))}
										</RadioGroup>
									</FormControl>
									<FormMessage />
								</FormItem>
							)}
						/>
						<div className="mt-10 mb-5 space-y-[2px]">
							<h5 className=" text-base font-semibold">Tema Sesuai Paket</h5>
							<p className="text-[#71717A] text-sm font-normal">Sentuh tab untuk memilih paket undangan</p>
						</div>
						<FormField
							control={form.control}
							name="tema"
							render={({ field }) => (
								<FormItem>
									<FormControl>
										<RadioGroup defaultValue="uio" className="grid gap-3">
											{state?.data?.map((data: any, index: number) => (
												<div key={index}>
													<RadioGroupItem value={data.tema} id={data.tema} onClick={() => setThemeChecked(data.tema)} className="peer sr-only" />
													<Label
														htmlFor={data.tema}
														className="flex flex-row w-full h-[72px] items-center space-x-4 rounded-2xl border-2 border-muted bg-popover p-4 hover:bg-accent hover:text-accent-foreground peer-data-[state=checked]:border-primary [&:has([data-state=checked])]:border-primary"
													>
														<div className="w-[38px] h-[38px] rounded-[13px] bg-slate-400">

														</div>
														<div className="flex flex-row w-full justify-between">
															<div className="flex flex-col peer">
																<h5 className="text-sm font-medium">{"Gold"}</h5>
																<p className="text-xs font-normal">{data.tema}</p>
															</div>

															<div className={`flex items-center ${themeChecked === data.tema ? "flex" : "hidden"}`}>
																<span className="aspect-square h-5 w-5 rounded-full border flex items-center justify-center bg-primary">
																	<CheckIcon className="h-4 w-4 text-white" />
																</span>
															</div>

														</div>
													</Label>
												</div>
											))}

										</RadioGroup>
									</FormControl>
									<FormMessage />
								</FormItem>
							)}
						/>
					</div>
					<div className="w-full h-[105px] px-[24px] pt-4 mb-auto z-50 sticky bottom-0 left-0 right-0 bg-opacity-80 backdrop-filter backdrop-blur-lg border-t border-[#F5F5F5] mt-[-105px]">
						<Button type="submit" size="primary">
							Selanjutnya
						</Button>
					</div>
				</form>
			</Form>

		</>
	)
}



