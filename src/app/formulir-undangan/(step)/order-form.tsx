"use client"

import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import * as z from "zod"

import { Button } from "@/components/ui/button"
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { toast } from "@/components/ui/use-toast"

interface PageProps {
    onNext: () => void;
}


const FormSchema = z.object({
    // name: z.string().min(2, {
    //     message: "Username must be at least 2 characters.",
    // }),
    // phone_number: z.string().min(2, {
    //     message: "Username must be at least 2 characters.",
    // }),
    // referral_code: z.string().min(2, {
    //     message: "Username must be at least 2 characters.",
    // }),
    // email: z.string().min(2, {
    //     message: "Username must be at least 2 characters.",
    // }),
    name: z.any(),
    referral_code: z.any(),
    email: z.any(),
    phone_number: z.any(),
    // phone_number: z.string().min(2, {
    //     message: "Username must be at least 2 characters.",
    // }),
    // referral_code: z.string().min(2, {
    //     message: "Username must be at least 2 characters.",
    // }),
    // email: z.string().min(2, {
    //     message: "Username must be at least 2 characters.",
    // }),
})

export function OrderForm({ onNext }: PageProps) {

    const handleNextClick = (): void => {
        console.log("biasaaa")
        // Lakukan validasi formulir jika diperlukan
        // ...

        // Panggil onNext untuk berpindah ke langkah berikutnya
        onNext();
    };


    const form = useForm<z.infer<typeof FormSchema>>({
        resolver: zodResolver(FormSchema),
        defaultValues: {
            name: "",
            phone_number: "",
            email: "",
            referral_code: "",
        },
    })

    function onSubmit(data: z.infer<typeof FormSchema>) {
        console.log("cek data", data)
        toast({
            title: "You submitted the following values:",
            description: (
                <pre className="mt-2 w-[340px] rounded-md bg-slate-950 p-4">
                    <code className="text-white">{JSON.stringify(data, null, 2)}</code>
                </pre>
            ),
        })
        handleNextClick()
    }

    return (
        <>
                <div className="pt-14 py-14 px-6">
                    <p className="text-center text-xs text-[#71717A] font-normal uppercase">STEP 1/10</p>
                    <h5 className="text-center text-md font-semibold uppercase">DATA PEMESAN</h5>
                    <p className="text-center text-[#71717A] text-sm font-normal">Input data pemesan dan gunakan<br />
                        voucher partnership</p>
                </div>
                <Form {...form}>
                    <form onSubmit={form.handleSubmit(onSubmit)}>
                        <div className="px-6 w-full space-y-4 pb-[150px]">
                        <FormField
                            control={form.control}
                            name="name"
                            render={({ field }) => (
                                <FormItem>
                                    <FormLabel>Nama</FormLabel>
                                    <FormControl>
                                        <Input placeholder="shadcn" {...field} />
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                        <FormField
                            control={form.control}
                            name="phone_number"
                            render={({ field }) => (
                                <FormItem>
                                    <FormLabel>No HP</FormLabel>
                                    <FormControl>
                                        <Input placeholder="shadcn" {...field} />
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                        <FormField
                            control={form.control}
                            name="email"
                            render={({ field }) => (
                                <FormItem>
                                    <FormLabel>Email</FormLabel>
                                    <FormControl>
                                        <Input placeholder="shadcn" {...field} />
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                        <div className="flex items-center pt-4 pb-2">
                            <hr className="flex-grow" />
                            <p className="mx-4 text-[10px]">PARTNERSHIP</p>
                            <hr className="flex-grow"></hr>

                        </div>
                        <div className="w-full h-[76px] flex flex-row p-4 bg-[#F8F8FB] gap-4 items-center rounded-md">
                            <div>
                                icon
                            </div>
                            <div>
                                <p className="text-xs font-light text-[#545764]">Gunakan kode refferal untuk mendapatkan potongan harga</p>
                            </div>
                        </div>
                        <FormField
                            control={form.control}
                            name="referral_code"
                            render={({ field }) => (
                                <FormItem>
                                    <FormLabel>Kode Referral</FormLabel>
                                    <FormControl>
                                        <Input placeholder="shadcn" {...field} />
                                    </FormControl>
                                    <FormMessage />
                                </FormItem>
                            )}
                        />

                        </div>
                        
                        <div className="w-full h-[105px] px-[24px] pt-4 mb-auto z-50 sticky bottom-0 left-0 right-0 bg-opacity-80 backdrop-filter backdrop-blur-lg border-t border-[#F5F5F5] mt-[-105px]">
                            <Button type="submit" size="primary" onClick={handleNextClick}>
                                Selanjutnya
                            </Button>
                        </div>
                    </form>
                </Form>
        
        </>
    )
}

