"use client"

import React, { useEffect, useState } from 'react'
import Container from '@/containers/invitation'
import Background from '@/components/Background'
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTiktok, faInstagram, faWhatsapp } from '@fortawesome/free-brands-svg-icons';


interface IndexProps {
  dataDetail: any; // Gantilah YourDataType dengan tipe yang sesuai
}


export default function index({ dataDetail }: IndexProps) {

  const dataImage = {
    coverImage: [
      'https://images.unsplash.com/photo-1676228003262-9a1596d5da14?q=80&w=2268&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
      'https://images.unsplash.com/photo-1557683304-673a23048d34?q=80&w=1700&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
      'https://plus.unsplash.com/premium_photo-1681774171279-7fbfa8760c26?q=80&w=2835&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
      'https://images.unsplash.com/photo-1436397543931-01c4a5162bdb?q=80&w=1919&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
      'https://plus.unsplash.com/premium_photo-1674984909531-7b3ac0035e60?q=80&w=2875&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    ],
    landingImage: [
      "https://plus.unsplash.com/premium_photo-1676009619407-18a5121f9687?q=80&w=2940&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
    ]
  }

  return (
    <>
      <div>

        <Container
          coverChild={
            <>
              <Background
                className={"bg-no-repeat bg-cover"}
                image={dataDetail.image_url}
                children={<p>adalah children</p>}
              />
            </>
          }
          invitationChild={
            <>
              COVER
              <section id='cover-invitation' className="bg-slate-800 w-full h-[100dvh]">
                <Background
                  className={"bg-no-repeat bg-cover object-cover bg-center"}
                  image={dataImage?.coverImage}
                  children={<p>adalah children</p>}
                />
              </section>
               {/* QUOTE */}
              <section id="quote-invitation" className="bg-zinc-700" >
                <p>sesi 2</p>
              </section>
               {/* DOA */}
              <section id="doa-invitation" className="bg-neutral-900">
                <p>sesi 3</p>
              </section>
               {/* FOOTER */}
              <section id="inv-footer" className="flex flex-col items-center justify-center pt-12 pb-12 font-['Open_Sans'] text-[10px] font-medium">
                <Image src="/assets/logo/logo-wdwu.svg" alt="Logo Wedding With You" width={100} height={100} />
                <div className='flex flex-row mt-[10px] mb-[5px] gap-4'>
                  <div className="flex flex-row gap-1 items-center">
                  <FontAwesomeIcon icon={faWhatsapp} />
                    <p>+085643700063</p>
                  </div>
                  <div className="flex flex-row gap-1 items-center">
                  <FontAwesomeIcon icon={faTiktok} />
                    <p>weddingwithyou</p>
                  </div>
                  <div className="flex flex-row gap-1 items-center">
                  <FontAwesomeIcon icon={faInstagram} />
                    <p>weddingwithyou.id</p>
                  </div>
                </div>
                <p>© all rights reserved by weddingwithyou.id</p>
              </section>
            </>
          }
        />
      </div>
    </>
  )
}
